<?php
/**
 * Created by PhpStorm.
 * User: Artjom
 * Date: 15.05.2018
 * Time: 7:55
 */
$connection = new PDO('sqlite:db1.sqlite');
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$contacts = $connection->prepare("SELECT * FROM contacts LEFT JOIN phoneNumbers ON contacts.id = phoneNumbers.contactId");
$contacts->execute();
$contactArray = [];
foreach ($contacts as $row) {
    if(sizeof($contactArray) == 0) {
        $tempArray = array($row['id'], $row['firstName'],$row['lastName'],$row['phone']);
        array_push($contactArray, $tempArray);
    }
    elseif (sizeof($contactArray !== 0)) {
        if(end($contactArray)[0] == $row['id']){
            $contactArray[sizeof($contactArray) - 1][3] = end($contactArray)[3] . "; " . $row['phone'];
        }
        if(end($contactArray)[0] !== $row['id']){
            $tempArray = array($row['id'], $row['firstName'],$row['lastName'],$row['phone']);
            array_push($contactArray, $tempArray);
        }
    }

}