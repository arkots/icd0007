<?php include_once 'writeToDatabase.php' ?>

<!DOCTYPE html>
<html>
<head>
	<title>Lisa kontakt</title>
	<link rel="stylesheet" type="text/css" href="Rakendus.css">
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
</head>
<body id="lisaBody">
<div id="container3">
	<form action="" method="post">
        <div id="error-block"><?php echo $char_error ?></div>
		<label>Eesnimi</label>
		<input name = "firstName" value="<?php echo $firstName;?>" type = "text" placeholder = "Eesnimi" autocomplete="off"/>
		<label>Perekonnanimi</label>
		<input name = "lastName" value="<?php echo $lastName ?>" type="text" placeholder="Perekonnanimi" autocomplete="off">
		<label>Telefoni number</label>
		<input name="phone1" type="text" value="<?php echo $phone1 ?>" placeholder="Telefoni number"
			   autocomplete="off">
		<input name="phone2" type="text" value="<?php echo $phone2 ?>" placeholder="Telefoni number"
			   autocomplete="off">
		<input name="phone3" type="text" value="<?php echo $phone3 ?>" placeholder="Telefoni number" autocomplete="off">

		<input name="submit-button" type="submit" value="Lisa uus kontakt">
	</form>
</div>

</body>
</html>
