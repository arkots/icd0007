<?php
/**
 * Created by PhpStorm.
 * User: Artjom
 * Date: 5.05.2018
 * Time: 14:06
 */
$char_error = "";
$firstName = $lastName = $phone1 = $phone2 = $phone3 = "";




if ((isset($_POST['firstName']) && isset($_POST['lastName'])) && (strlen((string)$_POST['firstName']) > 1 && strlen((string)$_POST['lastName']) > 2 )) {
    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $phone1 = $_POST["phone1"];
    $phone2 = $_POST["phone2"];
    $phone3 = $_POST["phone3"];
    $value1 = 1;
    $value2 = 2;
    $value3 = 3;


    $connection = new PDO('sqlite:db1.sqlite');
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $connection->prepare("INSERT INTO contacts (firstName, lastName) VALUES(:firstName,:lastName)");
    $stmt->bindValue(':firstName', $firstName);
    $stmt->bindValue(':lastName', $lastName);
    $stmt->execute();
    $idNumber = $connection ->lastInsertId();


    $stmt2 = $connection->prepare("INSERT INTO phoneNumbers (phone, contactId, phoneId) VALUES(:phone,:idNumber,$value1)");
    $stmt2->bindValue(':phone', $phone1);
    $stmt2->bindValue(':idNumber', $idNumber);
    $stmt2->execute();


    $stmt3 = $connection->prepare("INSERT INTO phoneNumbers (phone, contactId, phoneId) VALUES(:phone,:idNumber,$value2)");
    $stmt3->bindValue(':phone', $phone2);
    $stmt3->bindValue(':idNumber', $idNumber);
    $stmt3->execute();


    $stmt4 = $connection->prepare("INSERT INTO phoneNumbers (phone, contactId, phoneId) VALUES(:phone,:idNumber,$value3)");
    $stmt4->bindValue(':phone', $phone3);
    $stmt4->bindValue(':idNumber', $idNumber);
    $stmt4->execute();

    header("Location: index.php");
    exit();
}
if ((isset($_POST['firstName']) && isset($_POST['lastName'])) && (strlen((string)$_POST['firstName']) < 2 || strlen((string)$_POST['lastName']) < 2 )) {
    $char_error = "The name and lastname have to be atleast 2 characters long";
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $phone1 = $_POST['phone1'];
    $phone2 = $_POST['phone2'];
    $phone3 = $_POST['phone3'];

}
