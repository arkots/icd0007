<?php
/**
 * Created by PhpStorm.
 * User: Artjom
 * Date: 18.05.2018
 * Time: 6:50
 */
$char_error = "";
$firstName = $lastName = $phone1 = $phone2 = $phone3 = "";


if ((isset($_POST['firstName']) && isset($_POST['lastName'])) && (strlen((string)$_POST['firstName']) > 1 && strlen((string)$_POST['lastName']) > 2 )) {
    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $phone1 = $_POST["phone1"];
    $phone2 = $_POST["phone2"];
    $phone3 = $_POST["phone3"];
    $post = $_POST['id'];

    $connection = new PDO('sqlite:db1.sqlite');
    $stmt = $connection->prepare("UPDATE contacts SET firstName = :firstName, lastName = :lastName WHERE id = :post");
    $stmt->bindValue(':firstName', $firstName);
    $stmt->bindValue(':lastName', $lastName);
    $stmt->bindValue(':post', $post);
    $stmt->execute();

    $connection = new PDO('sqlite:db1.sqlite');
    $stmt = $connection->prepare("UPDATE phoneNumbers SET phone = :phone1 WHERE contactId = :post AND phoneId = 1");
    $stmt->bindValue(':phone1', $phone1);
    $stmt->bindValue(':post', $post);
    $stmt->execute();

    $connection = new PDO('sqlite:db1.sqlite');
    $stmt = $connection->prepare("UPDATE phoneNumbers SET phone = :phone2 WHERE contactId = :post AND phoneId = 2");
    $stmt->bindValue(':phone2', $phone2);
    $stmt->bindValue(':post', $post);
    $stmt->execute();

    $connection = new PDO('sqlite:db1.sqlite');
    $stmt = $connection->prepare("UPDATE phoneNumbers SET phone = :phone3 WHERE contactId = :post AND phoneId = 3");
    $stmt->bindValue(':phone3', $phone3);
    $stmt->bindValue(':post', $post);
    $stmt->execute();



    header("Location: index.php");
    exit();
}
if ((isset($_POST['firstName']) && isset($_POST['lastName'])) && (strlen((string)$_POST['firstName']) < 2 || strlen((string)$_POST['lastName']) < 2 )) {
    $char_error = "The name and lastname have to be atleast 2 characters long";
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $phone1 = $_POST['phone1'];
    $phone2 = $_POST['phone2'];
    $phone3 = $_POST['phone3'];

}
