<?php include_once 'readFromDatabase.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Nimekiri</title>
	<link rel="stylesheet" type="text/css" href="Rakendus.css">
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
	<div id="container1">
		<table>
			<tr>
				<td id="nimekiri">
					<i class="material-icons" style="font-size: 40px;">people</i><a id="list-page-link" href="?page=nimekiri">Nimekiri</a>
				</td>
				<td id="lisa">
					<i class="material-icons" style="font-size: 40px;">person_add</i>
					<a id="add-page-link" href="?page=addContact">Lisa</a>
				</td>
			</tr>
		</table>
	<hr>
	</div>
	<div id="container2">
		<table>
			<tr>
				<td id="nimi">Nimi</td>
				<td id="perenimi">Perekonnanimi</td>
				<td id="number">Telefoni number</td>
			</tr>
			<?php if ($contactArray): ?>
            <?php foreach ($contactArray as $contact): ?>
			<tr>
				<td> <?php echo "<a href='?page=updateContact&id={$contact[0]}'>{$contact[1]}</a>" ?></td>
				<td><?php echo $contact[2]; ?></td>
				<td><?php echo $contact[3]; ?></td>
			</tr>
            <?php endforeach; ?>
            <?php endif; ?>
		</table>
	</div>
	
</body>
</html>
